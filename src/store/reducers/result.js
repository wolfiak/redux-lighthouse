import * as actionT from '../actions/actionTypes';
import {updateObject} from '../utilty';

const initalState={
    results: []
}

const deleteResult= (state,action)=>{
    const updatedArray=state.results.filter((elm)=>{
        return elm.id !== action.id
    });
    return updateObject(state, {results: updatedArray});
}


const reducer=(state = initalState, action) =>{

    switch(action.type){
        case(actionT.STORE_RESULT):{
            return updateObject(state, {results: state.results.concat({id: new Date(), value: action.result })})
        }
        case(actionT.DELETE_RESULT):{
          return deleteResult(state,action);

        }
        default: {
            return state;
        }
    }

   
};

export default reducer;