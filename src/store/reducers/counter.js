import * as actionT from '../actions/actionTypes';
import { updateObject } from '../utilty';

const initalState={
    counter: 0

}
const reducer=(state = initalState, action) =>{

    switch(action.type){
        case(actionT.INCREMENT): {
            return updateObject(state, {counter: state.counter +1});
        }
        case(actionT.DECREMENT):{
            return updateObject(state, {counter: state.counter-1});
           
        }
        case(actionT.ADD):{
            return updateObject(state, {counter: state.counter+action.value});
        
        }
        case(actionT.SUB):{
            return updateObject(state, {counter: state.counter-action.value})
         
        }
        default: {
            return state;
        }
    }

   
};

export default reducer;