import * as actionTypes from './actionTypes';

export const saveResult= (result)=>{
    const update= result * 2;
    return{
        type: actionTypes.STORE_RESULT,
        result: update
    }
}
export const storeResult = (result)=> {
    return (dispatch, getState) => {
        setTimeout(()=>{
            console.log(getState().counter.counter);
            dispatch(saveResult(result));
        },2000);
};
}

export const deleteResult = (id)=> {
    return {
        type: actionTypes.DELETE_RESULT,
        id: id
    }
};