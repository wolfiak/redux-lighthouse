const redux=require('redux');
const createStore=redux.createStore;
//Reducer

const initialState={
    counter: 0
}

const rootReducer=(state = initialState,action)=>{
    switch(action.type){
        case ('INC_COUNTER'):{
           
            return{
                counter: state.counter+1
            }
            break;
        }
        case ('ADD_COUNTER'):{
       
            return{
                counter: state.counter+action.value
            }
            break;
        }
    }
    return state;
}
//Store
const store=createStore(rootReducer);

//Substraction

store.subscribe(()=>{
    console.log('[Subs]: ',store.getState());
});


//Dispatching Action
store.dispatch({type: 'INC_COUNTER'});
store.dispatch({type: 'ADD_COUNTER',value: 10});

